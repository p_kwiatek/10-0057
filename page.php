<div class="main-text">
<h2 class="main-header"><span><?php echo $pageName?></span></h2>
<?php
echo $message;

if ($showLoginForm)
{
    include( CMS_TEMPL . DS . 'form_login.php');
}
	
if ($showPage)
{
    echo $row['text'];
		
    if (! check_html_text($row['author'], '') )
    {
	?>
	<div class="authorName"><?php echo __('author'); ?>: <span><?php echo $row['author']?></span></div>
	<?php
    }
			
    /*
     * Articles
     */
    if ($numArticles > 0)
    {	
	$i = 0;
	?>		
	<div class="article-wrapper">
	    <?php
	    foreach ($outRowArticles as $row)
            {
		$highlight = $url = $target = $url_title = $protect = '';
			
		if ($row['protected'] == 1)
		{
        $protect = '<svg class="icon__protected" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10.031" height="15.312" viewBox="0 0 10.031 15.312"><defs><filter id="lock-filter-' . $i . '" x="801" y="489.688" width="10.031" height="15.312" filterUnits="userSpaceOnUse"><feOffset result="offset" dy="1" in="SourceAlpha"/><feGaussianBlur result="blur"/><feFlood result="flood" flood-color="#0d1b2d"/><feComposite result="composite" operator="in" in2="blur"/><feBlend result="blend" in="SourceGraphic" in2="SourceGraphic" /></filter></defs><path filter="url(#lock-filter-' . $i . ')" d="M807.1,500.062a1.075,1.075,0,1,1-1.075-1.074A1.075,1.075,0,0,1,807.1,500.062Zm2.506-3.938v-2.865a3.581,3.581,0,0,0-7.161,0v2.865a1.432,1.432,0,0,0-1.432,1.431v5.013A1.434,1.434,0,0,0,802.447,504h7.161a1.434,1.434,0,0,0,1.432-1.432v-5.013A1.433,1.433,0,0,0,809.608,496.124Zm-5.729-2.865a2.149,2.149,0,0,1,4.3,0v2.865h-4.3v-2.865Zm-1.432,9.309v-5.013h7.161v5.013h-7.162Z" transform="translate(-801 -489.688)"/></svg>';
        $url_title = ' title="' . __('page requires login') . '"';
		}				
				
		if (trim($row['ext_url']) != '')
		{
		    if ($row['new_window'] == '1')
		    {
			$target = ' target="_blank"';
		    }	
		    $url_title = ' title="' . __('opens in new window') . '"';
		    $url = ref_replace($row['ext_url']);					
		} else
		{
		    if ($row['url_name'] != '')
		    {
			$url = 'art,' . $row['id_art'] . ',' . $row['url_name'];
		    } else
		    {
			$url = 'index.php?c=article&amp;id=' . $row['id_art'];
		    }
		}	
				
		$margin = ' no-photo';
		if (is_array($photoLead[$row['id_art']]))
		{
		    $margin = '';
		}				
				
		$row['show_date'] = substr($row['show_date'], 0, 10);
		
		$highlight = '';
		if ($row['highlight'] == 1)
		{
		    $highlight = ' highlight-article';
		}			
		?>
        <div class="article<?php echo $highlight?><?php if (!is_array($photoLead[$row['id_art']])): ?> no-photo<?php endif; ?>">
            <div class="lead-text<?php echo $margin; ?>">
                <h4 class="article-title <?php echo $margin?>">
                    <a href="<?php echo $url?>" <?php echo $url_title . $target ?>><?php echo $row['name'] . $protect?></a>
                </h4>
                <div class="lead-main-text">
                    <?php echo truncate_html($row['lead_text'], 300, '...')?>
                </div>
                <div class="article-meta">
                    <a href="<?php echo $url ?>" <?php echo $url_title . $target ?> class="button" title="">
                        <span><?php echo __('more') ?></span>
                        <span class="sr-only"> <?php echo __('about')?>: <?php echo $row['name']; ?></span>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="9px" height="14px">
                            <defs>
                                <filter filterUnits="userSpaceOnUse" id="<?php echo 'article-arrow-right-' . $i; ?>" x="0px" y="0px" width="9px" height="14px"  >
                                    <feOffset in="SourceAlpha" dx="0" dy="1" />
                                    <feGaussianBlur result="blurOut" stdDeviation="0" />
                                    <feFlood flood-color="rgb(188, 225, 253)" result="floodOut" />
                                    <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                    <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                    <feMerge>
                                    <feMergeNode/>
                                    <feMergeNode in="SourceGraphic"/>
                                  </feMerge>
                                </filter>
                            </defs>
                            <g filter="<?php echo 'url(#article-arrow-right-' . $i . ')'; ?>">
                                <path fill-rule="evenodd" d="M-0.004,12.996 L6.493,6.499 L-0.004,0.002 L2.507,0.002 L9.004,6.499 L2.507,12.996 L-0.004,12.996 Z"/>
                            </g>
                        </svg>
                    </a>
                    <?php if ($row['show_date'] != '' && $row['show_date'] != '0000-00-00') { ?>
                        <p class="article-date">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 13 13">
                                <path fill-rule="evenodd" d="M10.768,7.295 L8.582,7.295 L8.582,5.110 L10.768,5.110 L10.768,7.295 ZM11.860,12.212 L2.027,12.212 C1.424,12.212 0.934,11.723 0.934,11.120 L0.934,2.925 C0.934,2.322 1.424,1.833 2.027,1.833 L3.120,1.833 L3.120,2.925 L2.027,2.925 L2.027,11.120 L11.860,11.120 L11.860,2.925 L10.221,2.925 L10.221,4.018 L9.129,4.018 L9.129,0.740 L10.221,0.740 L10.221,1.833 L11.860,1.833 C12.464,1.833 12.953,2.322 12.953,2.925 L12.953,11.120 C12.953,11.723 12.464,12.212 11.860,12.212 ZM8.036,10.027 L5.851,10.027 L5.851,7.842 L8.036,7.842 L8.036,10.027 ZM5.851,5.110 L8.036,5.110 L8.036,7.295 L5.851,7.295 L5.851,5.110 ZM4.758,4.018 L3.666,4.018 L3.666,0.740 L4.758,0.740 L4.758,1.833 L8.582,1.833 L8.582,2.925 L4.758,2.925 L4.758,4.018 ZM5.305,10.027 L3.120,10.027 L3.120,7.842 L5.305,7.842 L5.305,10.027 ZM3.120,5.110 L5.305,5.110 L5.305,7.295 L3.120,7.295 L3.120,5.110 Z"/>
                            </svg>
                            <span><?php echo $row['show_date'] ?></span>    
                        </p>
                    <?php } ?>
                </div>
            </div>
            <?php
                $columnWidth = 'col-sm-12';
                if (is_array($photoLead[$row['id_art']]))
                {
                    $photo = $photoLead[$row['id_art']];
                    $columnWidth = 'col-sm-offset-5 col-sm-7';
                    ?>
                    <div class="photo-wrapper<?php echo $photoWrapper; ?>">
                        <a href="files/<?php echo $lang?>/<?php echo $photo['file']?>" title="<?php echo __('enlarge image') . ': ' . $row['name']?>" class="photo fancybox" data-fancybox-group="gallery">
                            <span class="sr-only"><?php echo __('enlarge image')?>: <?php echo $row['name']?></span>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 360 240" class="photo-wrapper__shadow">
                                <path fill-rule="evenodd"  opacity="0.302"
                             d="M10.000,-0.000 L360.000,-0.000 L360.000,240.000 L-0.000,240.000 C6.000,191.000 10.000,-0.000 10.000,-0.000 Z"/>
                            </svg>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 360 250" class="photo-wrapper__image">
                                <defs>
                                    <pattern id="<?php echo 'article-image-' . $i; ?>" patternUnits="userSpaceOnUse" preserveAspectRatio="xMidYMid slice" width="100%" height="100%" x="0" y="0">
                                        <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $photo['file']; ?>"></image>
                                    </pattern>
                                </defs>
                                <path fill-rule="evenodd" fill="<?php echo 'url(#article-image-' . $i . ')'; ?>" d="M-0.000,-0.000 L360.000,-0.000 L360.000,250.000 L-0.000,250.000 L-0.000,-0.000 Z"/>
                            </svg>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="photo-wrapper__paperclip--shadow">
                                <path d="M74.2,62.5c-1.7,3.7-5.6,5.7-9.5,5.2l0.3-2.1c3,0.4,5.9-1.2,7.3-4   c0.8-1.7,0.9-3.6,0.2-5.3c-0.6-1.8-1.9-3.1-3.6-3.9L34,36.3c-1.2-0.6-2.5-0.6-3.8-0.1c-1.2,0.5-2.2,1.4-2.8,2.6   c-1.1,2.5-0.1,5.4,2.4,6.5l27.4,12.6c1.4,0.7,3.2,0,3.8-1.4c0.7-1.4,0-3.2-1.4-3.8L42,44.5c-0.5-0.2-0.8-0.9-0.5-1.4   c0.2-0.5,0.9-0.8,1.4-0.5l17.6,8.1c2.5,1.2,3.6,4.1,2.5,6.7c-1.2,2.5-4.1,3.6-6.7,2.5L28.9,47.2c-1.8-0.8-3-2.3-3.7-4   c-0.6-1.7-0.6-3.6,0.2-5.4c1.6-3.5,5.8-5.1,9.4-3.5l34.8,16.1c2.2,1,3.9,2.8,4.7,5.1C75.3,57.8,75.2,60.3,74.2,62.5z"/>
                            </svg>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="photo-wrapper__paperclip">
                                <path d="M74.2,62.5c-1.7,3.7-5.6,5.7-9.5,5.2l0.3-2.1c3,0.4,5.9-1.2,7.3-4   c0.8-1.7,0.9-3.6,0.2-5.3c-0.6-1.8-1.9-3.1-3.6-3.9L34,36.3c-1.2-0.6-2.5-0.6-3.8-0.1c-1.2,0.5-2.2,1.4-2.8,2.6   c-1.1,2.5-0.1,5.4,2.4,6.5l27.4,12.6c1.4,0.7,3.2,0,3.8-1.4c0.7-1.4,0-3.2-1.4-3.8L42,44.5c-0.5-0.2-0.8-0.9-0.5-1.4   c0.2-0.5,0.9-0.8,1.4-0.5l17.6,8.1c2.5,1.2,3.6,4.1,2.5,6.7c-1.2,2.5-4.1,3.6-6.7,2.5L28.9,47.2c-1.8-0.8-3-2.3-3.7-4   c-0.6-1.7-0.6-3.6,0.2-5.4c1.6-3.5,5.8-5.1,9.4-3.5l34.8,16.1c2.2,1,3.9,2.8,4.7,5.1C75.3,57.8,75.2,60.3,74.2,62.5z"/>
                            </svg>
                        </a>
                    </div>
                    <?php
                }   
            ?>
        </div>
            <?php		
	    }
	    ?>
	    	
        <?php

	$url = $PHP_SELF.'?c=' . $_GET['c'] . '&amp;id=' . $_GET['id'] . '&amp;s=';
	include (CMS_TEMPL . DS . 'pagination.php');
	?>
	</div>
	<?php
    }		
				
        /*
	 * Wypisanie plikow do pobrania
	 */
    if ($numFiles > 0)
    {	
    ?>
	<div class="files-wrapper row">
            <div class="col-xs-12">
                <h3 class="files-header"><?php echo __('files')?></h3>
                <ul class="list-unstyled">
                <?php
                foreach ($outRowFiles as $row)
                {
                    $target = 'target="_blank" ';

                    if (filesize('download/'.$row['file']) > 5000000)
                    {
                        $url = 'download/'.$row['file'];
                    } else
                    {
                        $url = 'index.php?c=getfile&amp;id='.$row['id_file'];
                    }
                    if (trim($row['name']) == '')
                    {
                        $name = $row['file'];
                    } else
                    {
                        $name = $row['name'];
                    }

                    $size = file_size('download/'.$row['file']);	
                    ?>		
                    <li>
                        <a href="<?php echo $url?>" <?php echo $target?>>
                            <i class="icon-doc-text-inv icon" aria-hidden="true"></i>
                            <span class="title">
                                <?php echo $name?>
                                <span class="size">(<?php echo $size?>)</span>
                            </span>                        
                        </a>                        
                    </li>
                    <?php
                }
                ?>
                </ul>
            </div>
	</div>
    <?php
    }
		
    /*
     *  Wypisanie zdjec
     */
    if ($numPhotos > 0)
    {	
	$i = 0;
	?>
	<div class="gallery-wrapper row">
        <div class="col-xs-12">
            <h3 class="gallery-header"><?php echo __('gallery')?></h3>
            <ul class="list-unstyled row gallery">
                <?php
                foreach ($outRowPhotos as $row)
                {
                    $i++;
                    ?>
                    <li class="col-sm-6 ">
                        <a href="files/<?php echo $lang?>/<?php echo $row['file']?>" title="<?php echo __('enlarge image') . ': ' . $row['name']?>" data-fancybox-group="gallery" class="photo fancybox">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 360 240" class="gallery-wrapper__shadow">
                                <path fill-rule="evenodd"  opacity="0.302"
                             d="M10.000,-0.000 L360.000,-0.000 L360.000,240.000 L-0.000,240.000 C6.000,191.000 10.000,-0.000 10.000,-0.000 Z"/>
                            </svg>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 360 250" class="gallery-wrapper__image">
                                <defs>
                                    <pattern id="<?php echo 'article-image-' . $i; ?>" patternUnits="userSpaceOnUse" preserveAspectRatio="xMidYMid slice" width="100%" height="100%" x="0" y="0">
                                        <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $row['file']?>"></image>
                                    </pattern>
                                </defs>
                                <path fill-rule="evenodd" fill="<?php echo 'url(#article-image-' . $i . ')'; ?>" d="M-0.000,-0.000 L360.000,-0.000 L360.000,250.000 L-0.000,250.000 L-0.000,-0.000 Z"/>
                            </svg>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="photo-wrapper__paperclip--shadow">
                                <path d="M74.2,62.5c-1.7,3.7-5.6,5.7-9.5,5.2l0.3-2.1c3,0.4,5.9-1.2,7.3-4   c0.8-1.7,0.9-3.6,0.2-5.3c-0.6-1.8-1.9-3.1-3.6-3.9L34,36.3c-1.2-0.6-2.5-0.6-3.8-0.1c-1.2,0.5-2.2,1.4-2.8,2.6   c-1.1,2.5-0.1,5.4,2.4,6.5l27.4,12.6c1.4,0.7,3.2,0,3.8-1.4c0.7-1.4,0-3.2-1.4-3.8L42,44.5c-0.5-0.2-0.8-0.9-0.5-1.4   c0.2-0.5,0.9-0.8,1.4-0.5l17.6,8.1c2.5,1.2,3.6,4.1,2.5,6.7c-1.2,2.5-4.1,3.6-6.7,2.5L28.9,47.2c-1.8-0.8-3-2.3-3.7-4   c-0.6-1.7-0.6-3.6,0.2-5.4c1.6-3.5,5.8-5.1,9.4-3.5l34.8,16.1c2.2,1,3.9,2.8,4.7,5.1C75.3,57.8,75.2,60.3,74.2,62.5z"/>
                            </svg>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="photo-wrapper__paperclip">
                                <path d="M74.2,62.5c-1.7,3.7-5.6,5.7-9.5,5.2l0.3-2.1c3,0.4,5.9-1.2,7.3-4   c0.8-1.7,0.9-3.6,0.2-5.3c-0.6-1.8-1.9-3.1-3.6-3.9L34,36.3c-1.2-0.6-2.5-0.6-3.8-0.1c-1.2,0.5-2.2,1.4-2.8,2.6   c-1.1,2.5-0.1,5.4,2.4,6.5l27.4,12.6c1.4,0.7,3.2,0,3.8-1.4c0.7-1.4,0-3.2-1.4-3.8L42,44.5c-0.5-0.2-0.8-0.9-0.5-1.4   c0.2-0.5,0.9-0.8,1.4-0.5l17.6,8.1c2.5,1.2,3.6,4.1,2.5,6.7c-1.2,2.5-4.1,3.6-6.7,2.5L28.9,47.2c-1.8-0.8-3-2.3-3.7-4   c-0.6-1.7-0.6-3.6,0.2-5.4c1.6-3.5,5.8-5.1,9.4-3.5l34.8,16.1c2.2,1,3.9,2.8,4.7,5.1C75.3,57.8,75.2,60.3,74.2,62.5z"/>
                            </svg>
                            <?php
                            if (! check_html_text($row['name'], '') ) {
                                ?>
                                <p class="photo-name" aria-hidden="true"><?php echo $row['name']?></p>
                                <?php
                            }
                            ?>
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
            <div class="clearfix"></div>
        </div>
	</div>
	<?php
    }		

    if ($outSettings['pluginTweet'] == 'włącz')
    {
	 echo '<div class="Tweet"><iframe frameborder="0" scrolling="no" src="//platform.twitter.com/widgets/tweet_button.html" style="width:80px; height:30px;"></iframe></div>';  
    }

    if ($outSettings['pluginFB'] == 'włącz')
    {
	$fb_url = urlencode('http://'.$pageInfo['host'].'/index.php?c=page&amp&id='. $_GET['id']);
	echo '<div class="FBLike"><iframe src=\'http://www.facebook.com/plugins/like.php?href='.$fb_url.'&amp;layout=standard&amp;show_faces=true&amp;width=400&amp;action=like&amp;font=tahoma&amp;colorscheme=light&amp;height=32&amp;show_faces=false\' scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:400px; height:32px;"></iframe></div>';   
}
}
?>
</div>