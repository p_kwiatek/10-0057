<nav id="toolbar" class="toolbar">
    <ul class="toolbar__links">
        <li class="home"><a href="index.php" title="<?php echo __("home page"); ?>"><img src="<?php echo $pathTemplate; ?>/images/home-icon.png" width="25" height="21" alt="<?php echo __("home page"); ?>" /></a></li>
        <li class="fonts">
            <ul>
                <li class="sr-only"><?php echo __('font size'); ?>:</li>
                <li>
                    <a href="ch_style.php?style=0">
                        <span class="sr-only"><?php echo __('font default')?></span>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12.25" height="13.813" viewBox="0 0 12.25 13.813">
                            <defs>
                                <filter id="filter-font-default" x="251.469" y="39.188" width="12.25" height="13.813" filterUnits="userSpaceOnUse">
                                    <feOffset result="offset" dy="1" in="SourceAlpha"/>
                                    <feGaussianBlur result="blur"/>
                                    <feFlood result="flood" flood-color="#28170b" flood-opacity="0.6"/>
                                    <feComposite result="composite" operator="in" in2="blur"/>
                                    <feBlend result="blend" in="SourceGraphic" in2="SourceGraphic" />
                                </filter>
                            </defs>
                            <path filter="url(#filter-font-default)" fill="#ee8825" d="M251.483,52h3.665V50.892l-0.966-.167,0.87-2.355h5.124l0.844,2.355-0.967.167V52h3.674V50.892l-0.941-.132L258.392,39.2h-1.486L252.433,50.76l-0.95.132V52Zm6.126-10.573h0.053l1.986,5.493h-4.06Z" transform="translate(-251.469 -39.188)"/>
                        </svg>
                    </a>
                </li>
                <li>
                    <a href="ch_style.php?style=r1">
                        <span class="sr-only"><?php echo __('font bigger')?></span>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="17.406" height="19" viewBox="0 0 17.406 19">
                            <defs>
                                <filter id="filter-font-bigger" x="272.594" y="34" width="17.406" height="19" filterUnits="userSpaceOnUse">
                                    <feOffset result="offset" dy="1" in="SourceAlpha"/>
                                    <feGaussianBlur result="blur"/>
                                    <feFlood result="flood" flood-color="#28170b" flood-opacity="0.6"/>
                                    <feComposite result="composite" operator="in" in2="blur"/>
                                    <feBlend result="blend" in="SourceGraphic" in2="SourceGraphic" />
                                </filter>
                            </defs>
                            <path filter="url(#filter-font-bigger)" fill="#ee8825" d="M272.591,52h4.479V50.646l-1.181-.2,1.063-2.879h6.263l1.031,2.879-1.182.2V52h4.491V50.646l-1.15-.161-5.371-14.126h-1.815l-5.468,14.126-1.16.161V52Zm7.487-12.923h0.064l2.428,6.714h-4.963ZM290,38h-2v2h-2V38h-2V36h2V34h2v2h2v2Z" transform="translate(-272.594 -34)"/>
                        </svg>
                    </a>
                </li>
                <li>
                    <a href="ch_style.php?style=r2">
                        <span class="sr-only"><?php echo __('font biggest')?></span>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="28.312" height="19.5" viewBox="0 0 28.312 19.5">
                            <defs>
                                <filter id="filter-font-biggest" x="296.688" y="33.5" width="28.312" height="19.5" filterUnits="userSpaceOnUse">
                                    <feOffset result="offset" dy="1" in="SourceAlpha"/>
                                    <feGaussianBlur result="blur"/>
                                    <feFlood result="flood" flood-color="#28170b" flood-opacity="0.6"/>
                                    <feComposite result="composite" operator="in" in2="blur"/>
                                    <feBlend result="blend" in="SourceGraphic" in2="SourceGraphic" />
                                </filter>
                            </defs>
                            <path filter="url(#filter-font-biggest)" fill="#ee8825" d="M296.7,52h5.294V50.4l-1.4-.241,1.257-3.4h7.4l1.219,3.4-1.4.241V52h5.307V50.4l-1.359-.19-6.347-16.694h-2.146L298.069,50.21l-1.371.19V52Zm8.849-15.272h0.076l2.869,7.935h-5.865ZM317,38h-2v2h-2V38h-2V36h2V34h2v2h2v2Zm8,0h-2v2h-2V38h-2V36h2V34h2v2h2v2Z" transform="translate(-296.688 -33.5)"/>
                        </svg>
                    </a>
                </li>
            </ul>
        </li>
        <?php
            if ($_SESSION['contr'] == 0) {
                $set_contrast = 1;
                $contrast_txt = __('contrast version');
            } else {
                $set_contrast = 0;
                $contrast_txt = __('graphic version');
            }
        ?>
        <li class="contrast">
            <span class="sr-only"><?php echo __('font contrast'); ?>:</span>
            <a href="ch_style.php?contr=<?php echo $set_contrast ?>">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 18 19">
                    <path d="M50,60c-5,0-9-4-9-9c0-5,4-9,9-9c5,0,9,4,9,9C59,56,55,60,50,60z M43.8,51 c0,3.4,2.8,6.2,6.2,6.2V44.8C46.6,44.8,43.8,47.6,43.8,51z" transform="translate(-41,-42)" />
                </svg>
            </a>
        </li>
        <li class="bip">
            <a href="<?php echo $pageInfo['bip'] ?>">
                <img src="<?php echo $pathTemplate; ?>/images/toolbar/bip.png" alt="<?php echo __('bip'); ?>">
            </a>
        </li>
    </ul>
    <div id="searchbar" class="toolbar__search">
        <a id="search" tabindex="-1" style="display: inline;"></a>
        <h2 class="sr-only"><?php echo __('search')?></h2>
        <form id="searchForm" name="f_szukaj" method="get" action="index.php">
            <input name="c" type="hidden" value="search" />
            <label for="kword"><span class="sr-only"><?php echo __('search query')?></span></label>
            <input type="text" id="kword" name="kword" value="<?php echo __('search query'); ?>" onfocus="if (this.value=='<?php echo __('search query'); ?>') {this.value=''};" onblur="if (this.value=='') {this.value='<?php echo __('search query'); ?>'};"/>
            <button type="submit" name="search" id="toolbar-search">
                <span class="sr-only"><?php echo __('search action'); ?></span>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="19.375" height="21" viewBox="0 0 19.375 21">
                    <defs>
                        <filter id="filter-search" x="465" y="85" width="19.375" height="21" filterUnits="userSpaceOnUse">
                            <feOffset result="offset" dy="1" in="SourceAlpha"/>
                            <feGaussianBlur result="blur"/>
                            <feFlood result="flood" flood-color="#28170b" flood-opacity="0.6"/>
                            <feComposite result="composite" operator="in" in2="blur"/>
                            <feBlend result="blend" in="SourceGraphic" in2="SourceGraphic" />
                        </filter>
                    </defs>
                    <path filter="url(#filter-search)" d="M484.368,102.52l-4.751-4.752a7.109,7.109,0,0,0,1.476-4.718,8.048,8.048,0,1,0-8.049,8.049,7.1,7.1,0,0,0,4.659-1.432l4.759,4.759Zm-14.954-5.84a5.133,5.133,0,1,1,3.63,1.5A5.1,5.1,0,0,1,469.414,96.68Z" transform="translate(-465 -85)"/>
                </svg>
            </button>
        </form>
    </div>
</nav>
