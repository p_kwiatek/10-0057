<?php if ($numRowBottomModules > 0): ?>
<div id="modules-bottom" class="modules-content modules-bottom">
    <div class="row">
        <?php
            $href = '/^<a.*?href=(["\'])(.*?)\1.*$/';
            
            $links = array();
            
            for ($i = 0; $i < $numRowBottomModules; $i++) {
                $tmp = get_module($outRowBottomModules[$i]['mod_name']);
                
                preg_match($href, $tmp, $m);

                if ($m[2] != '') {
                    $links[] = $m[2];
                } else {
                    $links[] = trans_url_name($outRowBottomModules[$i]['name']);
                }
            }
            
            $modules_color2 = array(
                'mod_forum',
            );
            
            $module_grid_classes = array(1 => "col-sm-12 col-md-12", 2 => "col-sm-6 col-md-6", 3 => "col-sm-4 col-md-4");
            $module_grid_class = array_key_exists($numRowBottomModules, $module_grid_classes) ? $module_grid_classes[$numRowBottomModules] : $module_grid_classes[3];
        ?>
        
        <?php for ($i = 0; $i < $numRowBottomModules; $i++): ?>
            <div class="<?php echo $module_grid_class; ?>">
                <a href="<?php echo $links[$i]; ?>" class="module module-common <?php echo in_array($outRowBottomModules[$i]['mod_name'], $modules_color2) ? 'color2' : ''; ?>" id="<?php echo $outRowBottomModules[$i]['mod_name']; ?>">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 263 217" class="modules-content__shadow">
                        <path fill-rule="evenodd" d="M161.253,5.348 C90.755,-12.380 20.478,19.430 4.284,76.397 C-11.910,133.364 32.112,193.916 102.609,211.644 C173.107,229.371 243.384,197.562 259.578,140.595 C275.772,83.628 231.750,23.076 161.253,5.348 Z"/>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 258 215" class="modules-content__image">
                        <defs>
                            <pattern id="<?php echo 'modules-content-image-' . $outRowBottomModules[$i]['mod_name']; ?>" patternUnits="userSpaceOnUse" preserveAspectRatio="xMidYMid slice" width="100%" height="100%" x="0" y="0">
                                <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/modules-content/' . $outRowBottomModules[$i]['mod_name'] . '.jpg' ?>"></image>
                            </pattern>
                        </defs>
                        <path d="M157.654,8.355 C88.595,-14.525 19.752,11.277 3.888,65.985 C-11.975,120.692 31.148,183.589 100.207,206.469 C169.266,229.349 238.110,203.547 253.973,148.840 C269.837,94.132 226.713,31.235 157.654,8.355 Z" fill="<?php echo 'url(#modules-content-image-' . $outRowBottomModules[$i]['mod_name'] . ')' ?>"/>
                    </svg>
                    <h2 class="module-name">
                        <span><?php echo $outRowBottomModules[$i]['name'] ?></span>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 255 55" class="modules-content__name-shadow">
                            <path d="M0.941,8.034 L251.990,0.054 L254.020,43.919 C178.392,46.860 70.152,50.123 4.486,54.804 C6.817,40.431 3.829,22.332 0.941,8.034 Z"/>
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 255 48" class="modules-content__name-background">
                            <path d="M0.941,1.034 L251.990,-0.002 L254.020,43.918 C178.392,44.769 70.152,45.037 4.486,47.902 C6.817,33.594 3.829,15.412 0.941,1.034 Z"/>
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="modules-content__paperclip--shadow">
                            <path d="M74.2,62.5c-1.7,3.7-5.6,5.7-9.5,5.2l0.3-2.1c3,0.4,5.9-1.2,7.3-4   c0.8-1.7,0.9-3.6,0.2-5.3c-0.6-1.8-1.9-3.1-3.6-3.9L34,36.3c-1.2-0.6-2.5-0.6-3.8-0.1c-1.2,0.5-2.2,1.4-2.8,2.6   c-1.1,2.5-0.1,5.4,2.4,6.5l27.4,12.6c1.4,0.7,3.2,0,3.8-1.4c0.7-1.4,0-3.2-1.4-3.8L42,44.5c-0.5-0.2-0.8-0.9-0.5-1.4   c0.2-0.5,0.9-0.8,1.4-0.5l17.6,8.1c2.5,1.2,3.6,4.1,2.5,6.7c-1.2,2.5-4.1,3.6-6.7,2.5L28.9,47.2c-1.8-0.8-3-2.3-3.7-4   c-0.6-1.7-0.6-3.6,0.2-5.4c1.6-3.5,5.8-5.1,9.4-3.5l34.8,16.1c2.2,1,3.9,2.8,4.7,5.1C75.3,57.8,75.2,60.3,74.2,62.5z"/>
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="modules-content__paperclip">
                            <path d="M74.2,62.5c-1.7,3.7-5.6,5.7-9.5,5.2l0.3-2.1c3,0.4,5.9-1.2,7.3-4   c0.8-1.7,0.9-3.6,0.2-5.3c-0.6-1.8-1.9-3.1-3.6-3.9L34,36.3c-1.2-0.6-2.5-0.6-3.8-0.1c-1.2,0.5-2.2,1.4-2.8,2.6   c-1.1,2.5-0.1,5.4,2.4,6.5l27.4,12.6c1.4,0.7,3.2,0,3.8-1.4c0.7-1.4,0-3.2-1.4-3.8L42,44.5c-0.5-0.2-0.8-0.9-0.5-1.4   c0.2-0.5,0.9-0.8,1.4-0.5l17.6,8.1c2.5,1.2,3.6,4.1,2.5,6.7c-1.2,2.5-4.1,3.6-6.7,2.5L28.9,47.2c-1.8-0.8-3-2.3-3.7-4   c-0.6-1.7-0.6-3.6,0.2-5.4c1.6-3.5,5.8-5.1,9.4-3.5l34.8,16.1c2.2,1,3.9,2.8,4.7,5.1C75.3,57.8,75.2,60.3,74.2,62.5z"/>
                        </svg>                  
                    </h2>
                </a>
            </div>
        <?php endfor; ?>
    </div>
</div>
<?php endif; ?>
