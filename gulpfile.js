var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var del = require('del');

gulp.task('sass', function() {
	return gulp.src('src/scss/style.scss')
		.pipe(sass({outputStyle: 'expanded'}))
		.pipe(gulp.dest('css'))
});

gulp.task('autoprefixer', ['sass'], function() {
	return gulp.src('css/style.css')
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest('css'))
});

gulp.task('copy', ['autoprefixer'], function() {
	return gulp.src(['src/**/*','!src/zen/**/*','!src/zen', '!src/scss/**/*', '!src/scss'])
		.pipe(gulp.dest('dist'));
});

gulp.task('watch', ['autoprefixer'], function() {
	gulp.watch('src/scss/**/*.scss', ['autoprefixer']);
});

gulp.task('default', function() {
	console.log("--------------------------------------------------------------------------------\nZEN 1.0.0 WEB:: Witamy na pokładzie, sir. \n-------------------------------------------------------------------------------- \ngulp:watch używaj do pracy z projektem, \ngulp:build, do tworzenia projektu gotowego do dystrybucji. \n--------------------------------------------------------------------------------");
});