<?php
if ($pagination['end'] > 1)
{
    ?>
<div class="row">
    <div class="pagination-wrapper col-xs-12">
        <?php
        $active_page = $pagination['active'];

        if ( !isset($pagination['active']) )
        {
            $active_page = 1;
        }
        ?>
        <p><?php echo __('page')?>: <strong><?php echo $active_page?></strong>/<?php echo $pagination['end']?></p>
        <ul class="list-unstyled list-inline text-center">
        <?php
        if ($pagination['start'] != $pagination['prev'])
        {
            ?>
            <li>
                <a href="<?php echo $url . $pagination['start']?>" rel="nofollow" class="btn-first">
                    <span class="sr-only"><?php echo __('first page')?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="14px">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-first-filter" x="0px" y="0px" width="14px" height="14px">
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(188, 225, 253)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                <feMerge>
                                <feMergeNode/>
                                <feMergeNode in="SourceGraphic"/>
                              </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-first-filter)">
                            <path fill-rule="evenodd" d="M7.507,12.996 L4.996,12.996 L11.493,6.499 L4.996,0.002 L7.507,0.002 L14.004,6.499 L7.507,12.996 ZM2.507,12.996 L-0.004,12.996 L6.493,6.499 L-0.004,0.002 L2.507,0.002 L9.004,6.499 L2.507,12.996 Z"/>
                        </g>
                    </svg>
                </a>
            </li>
            <li>
                <a href="<?php echo $url . $pagination['prev']?>" rel="nofollow" class="btn-prev">
                    <span class="sr-only"><?php echo __('prev page')?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="9px" height="14px">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-prev-filter" x="0px" y="0px" width="9px" height="14px">
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(188, 225, 253)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                <feMerge>
                                <feMergeNode/>
                                <feMergeNode in="SourceGraphic"/>
                              </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-prev-filter)">
                            <path fill-rule="evenodd" d="M-0.004,12.996 L6.493,6.499 L-0.004,0.002 L2.507,0.002 L9.004,6.499 L2.507,12.996 L-0.004,12.996 Z"/>
                        </g>
                    </svg>
                </a>
            </li>
            <?php
        }		
        foreach ($pagination as $k => $v)
        {
            if (is_numeric($k))
            {
                ?>
            <li>
                <a href="<?php echo $url . $v?>" rel="nofollow" class="btn-page">
                    <span class="sr-only"><?php echo __('page')?></span>
                    <?php echo $v?>
                </a>
            </li>
                <?php
            } else if ($k == 'active')
            {
                ?>
            <li>
                <span class="page-active">
                    <span class="sr-only"><?php echo __('page')?></span>
                    <?php echo $v?>
                </span>
            </li>
                <?php
            }			
        }		
        if ($pagination['active'] != $pagination['end'])
        {
            ?>
            <li>
                <a href="<?php echo $url . $pagination['next']?>" rel="nofollow" class="btn-next">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="9px" height="14px">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-next-filter" x="0px" y="0px" width="9px" height="14px"  >
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(188, 225, 253)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                <feMerge>
                                <feMergeNode/>
                                <feMergeNode in="SourceGraphic"/>
                              </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-next-filter)">
                            <path fill-rule="evenodd" d="M-0.004,12.996 L6.493,6.499 L-0.004,0.002 L2.507,0.002 L9.004,6.499 L2.507,12.996 L-0.004,12.996 Z"/>
                        </g>
                    </svg>
                    <span class="sr-only"><?php echo __('next page')?></span>
                </a>
            </li>
            <li>
                <a href="<?php echo $url . $pagination['end']?>" rel="nofollow" class="btn-last">
                    <span class="sr-only"><?php echo __('last page')?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="14px">
                        <defs>
                            <filter filterUnits="userSpaceOnUse" id="pagination-last-filter" x="0px" y="0px" width="14px" height="14px">
                                <feOffset in="SourceAlpha" dx="0" dy="1" />
                                <feGaussianBlur result="blurOut" stdDeviation="0" />
                                <feFlood flood-color="rgb(188, 225, 253)" result="floodOut" />
                                <feComposite operator="atop" in="floodOut" in2="blurOut" />
                                <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                <feMerge>
                                <feMergeNode/>
                                <feMergeNode in="SourceGraphic"/>
                              </feMerge>
                            </filter>
                        </defs>
                        <g filter="url(#pagination-last-filter)">
                            <path fill-rule="evenodd" d="M7.507,12.996 L4.996,12.996 L11.493,6.499 L4.996,0.002 L7.507,0.002 L14.004,6.499 L7.507,12.996 ZM2.507,12.996 L-0.004,12.996 L6.493,6.499 L-0.004,0.002 L2.507,0.002 L9.004,6.499 L2.507,12.996 Z"/>
                        </g>
                    </svg>
                </a>
            </li>
            <?php
        }
        ?>	
        </ul>
        <div class="clearfix"></div>
    </div>
</div>
    <?php
}
?>