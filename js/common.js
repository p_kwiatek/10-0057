/*
 * Calendar reload
 */

$(document).on("click", ".caption_nav_prev a", function ()
{
    var date = $(this).attr('id').substr(2);
    $('#mod_calendar2 .module_calendar').load('index.php?c=get_calendar&date=' + date);

    $('#calendarNews').load('index.php?c=get_calendar&date=' + date);
});

$(document).on("click", ".caption_nav_next a", function ()
{
    var date = $(this).attr('id').substr(2);
    $('#mod_calendar2 .module_calendar').load('index.php?c=get_calendar&date=' + date);

    $('#calendarNews').load('index.php?c=get_calendar&date=' + date);
});

$(document).ready(function () {
    
    var isMobileView = false;
    var isLargeView = false;
    var isMediumView = false;
    var isModuleHeightAlign = false;

    $('.footer__gotop').on('click', function(e) {
        e.preventDefault();
        $('html,body').animate({ scrollTop: 0 }, 'slow');
        $('.page').focus();
        return false; 
    });

    $('.icon-protected, .protectedPage').each(function(index) {
        $('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10.031" height="15.312" viewBox="0 0 10.031 15.312"><defs><filter id="lock-filter-icon-' + index + '" x="801" y="489.688" width="10.031" height="15.312" filterUnits="userSpaceOnUse"><feOffset result="offset" dy="1" in="SourceAlpha"/><feGaussianBlur result="blur"/><feFlood result="flood" flood-color="#0d1b2d"/><feComposite result="composite" operator="in" in2="blur"/><feBlend result="blend" in="SourceGraphic" in2="SourceGraphic" /></filter></defs><path filter="url(#lock-filter-icon-' + index + ')" d="M807.1,500.062a1.075,1.075,0,1,1-1.075-1.074A1.075,1.075,0,0,1,807.1,500.062Zm2.506-3.938v-2.865a3.581,3.581,0,0,0-7.161,0v2.865a1.432,1.432,0,0,0-1.432,1.431v5.013A1.434,1.434,0,0,0,802.447,504h7.161a1.434,1.434,0,0,0,1.432-1.432v-5.013A1.433,1.433,0,0,0,809.608,496.124Zm-5.729-2.865a2.149,2.149,0,0,1,4.3,0v2.865h-4.3v-2.865Zm-1.432,9.309v-5.013h7.161v5.013h-7.162Z" transform="translate(-801 -489.688)"/></svg>').appendTo(this);
    });

    if ($('.modules-top #mod_calendar').length > 0) {
        $('.clock-container').appendTo('.modules-top #mod_calendar .modules-top__icon');
    }

    else if ($('.sidebar-modules #mod_calendar').length > 0) {
        $('.clock-container').append('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 73 73" class="clock-container__background"><defs><filter id="sidebar-mod-calendar-filter"><feOffset in="SourceAlpha" dx="-4.95" dy="4.95" /><feGaussianBlur result="blurOut" stdDeviation="0" /><feFlood flood-color="rgb(12, 53, 96)" result="floodOut" /><feComposite operator="out" in="floodOut" in2="blurOut" result="compOut" /><feComposite operator="in" in="compOut" in2="SourceAlpha" /><feComponentTransfer><feFuncA type="linear" slope="0.5"/></feComponentTransfer><feBlend mode="normal" in2="SourceGraphic" /></filter></defs><g filter="url(#sidebar-mod-calendar-filter)"><path fill-rule="evenodd" d="M39.025,0.017 C57.779,0.017 72.983,15.220 72.983,33.975 C72.983,52.729 57.779,67.932 39.025,67.932 C20.271,67.932 5.068,52.729 5.068,33.975 C5.068,15.220 20.271,0.017 39.025,0.017 Z"/></g></svg>');
    }

    if ($('.sidebar-modules #mod_programs').length > 0) {
        $('#mod_programs').addClass('module--icon');
        $('<div class="icon"></div>').prependTo('#mod_programs .module-body');
    }
    
    $('#btnFilePos').on('click', function() {
        $('#avatar_f').trigger('click');
    });

    $('input[type="submit"], .button, .butForm').wrap('<div class="button-wrapper"></div>');

    $('.menu-top .dropdown-submenu a.selected').closest('.dropdown-submenu').children('a').addClass('selected');

    $('.sidebar-menu li a').each(function(index) {
        $('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="sidebar-menu__point" width="20px" height="22px"><defs><filter id="sidebar-menu-li-' + index + '"><feOffset in="SourceAlpha" dx="0" dy="2" /><feGaussianBlur result="blurOut" stdDeviation="0" /><feFlood flood-color="rgb(167, 176, 185)" result="floodOut" /><feComposite operator="out" in="floodOut" in2="blurOut" result="compOut" /><feComposite operator="in" in="compOut" in2="SourceAlpha" /><feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer><feBlend mode="normal" in2="SourceGraphic" /></filter></defs><g filter="url(#sidebar-menu-li-' + index + ')"><path fill-rule="evenodd" d="M9.520,-0.000 C14.778,-0.000 19.040,4.262 19.040,9.520 C19.040,14.778 14.778,19.040 9.520,19.040 C4.262,19.040 -0.000,14.778 -0.000,9.520 C-0.000,4.262 4.262,-0.000 9.520,-0.000 Z"/></g></svg>').appendTo(this);
    });

    if (isMobile.any) {
        $('body').addClass('is-mobile');

        $('.dropdown-submenu', '#navbar-top').on('hide.bs.dropdown', function () {
            return false;
        });
    }

    $(".board table, .main-text table").each(function() {
        var $table = $(this);
        
        if ($table.parent('.table-responsive').length === 0) {
            $table.wrap('<div class="table-responsive"></div>');
        }
    });

    $(".owl-carousel").owlCarousel({
        singleItem: true,
        autoPlay: 1e3 * settings.duration,
        slideSpeed: 1e3 * settings.animationDuration,
        paginationSpeed: 1e3 * settings.animationDuration,
        transitionStyle: settings.transition,
        pagination: false,
        mouseDrag: false,
        touchDrag: false
    });

    function equalizeHeights(selector) {
        var heights = new Array();
        $(selector).each(function() {
            $(this).css('min-height', '0');
            $(this).css('max-height', 'none');
            $(this).css('height', 'auto');
            heights.push($(this).height());
        });

        var max = Math.max.apply(Math, heights);

        $(selector).each(function() {
            $(this).css('height', max + 'px');
        }); 
    }

    function equalizeHeightsWithPadding(selector) {
        var heights = new Array();
        $(selector).each(function() {
            $(this).css('min-height', '0');
            $(this).css('max-height', 'none');
            $(this).css('height', 'auto');
            heights.push($(this).outerHeight());
        });

        var max = Math.max.apply( Math, heights );

        $(selector).each(function() {
            $(this).css('height', max + 'px');
        }); 
    }

    function fancyboxInit() {
        var a;
        $("a.fancybox").fancybox({
            overlayOpacity: .9,
            overlayColor: settings.overlayColor,
            titlePosition: "outside",
            titleFromAlt: !0,
            titleFormat: function (a, b, c, d) {
                return '<span id="fancybox-title-over">' + texts.image + " " + (c + 1) + " / " + b.length + "</span>" + (a.length ? " &nbsp; " + a.replace(texts.enlargeImage + ": ", "") : "")
            },
            onStart: function (b, c, d) {
                a = b[c];
            },
            onComplete: function () {
            },
            onClosed: function () {
            }
        });
    }

    fancyboxInit();

    function watch() {
        var s = Snap(document.getElementById("clock"));
        var seconds = s.select("#handSecond"),
            minutes = s.select("#handMinute"),
            hours = s.select("#handHour"),
            face = {
                elem: s.select("#face"),
                cx: s.select("#face").getBBox().cx,
                cy: s.select("#face").getBBox().cy
            },
            angle = 0;
        function update() {
            var time = new Date();
            setHours(time);
            setMinutes(time);
            setSeconds(time);
        }
        function setHours(t) {
            var hour = t.getHours();
            hour %= 12;
            hour += Math.floor(t.getMinutes()/10)/6;
            var angle = hour*360/12;
            hours.animate(
                {transform: "rotate("+angle+" "+face.cx+" "+face.cy+")"},
                100,
                mina.linear,
                function(){
                    if (angle === 360) {
                        hours.attr({transform: "rotate("+0+" "+face.cx+" "+face.cy+")"});               
                    }
                }
              );
        }
        function setMinutes(t) {
            var minute = t.getMinutes();
            minute %= 60;
            minute += Math.floor(t.getSeconds()/10)/6;
            var angle = minute*360/60;
            minutes.animate(
                {transform: "rotate("+angle+" "+face.cx+" "+face.cy+")"},
                100,
                mina.linear,
                function() {
                    if (angle === 360) {
                        minutes.attr({transform: "rotate("+0+" "+face.cx+" "+face.cy+")"});
                    }
                }
            );
        }
        function setSeconds(t) {
            t = t.getSeconds();
            t %= 60;
            var angle = t*360/60;
            if (angle === 0) angle = 360;
            seconds.animate(
                {transform: "rotate("+angle+" "+face.cx+" "+face.cy+")"},
                600,
                mina.elastic,
                function(){
                    if (angle === 360) {
                        seconds.attr({transform: "rotate("+0+" "+face.cx+" "+face.cy+")"});     
                    }
                }
            );
        }
        update();
        setInterval(update, 1000);
    }

    if ($('#face').length > 0) {
        watch();
    }

    if (popup.show)
    {
        $.fancybox(
                popup.content,
                {
                    overlayOpacity: .9,
                    overlayColor: settings.overlayColor,
                    padding: 20,
                    autoDimensions: !1,
                    width: popup.width,
                    height: popup.height,
                    transitionIn: "fade",
                    transitionOut: "fade",
                    onStart: function (a) {
                        $("#fancybox-outer").css({
                            background: popup.popupBackground
                        });
                        $("#fancybox-content").addClass("main-text no-margin");
                    }
                }
        );
    }

    $('.toolbar__search').on('click', 'button', function (e) {
        if (Modernizr.mq('(max-width: 991px)')) {
            e.stopPropagation();

            if (!$(e.delegateTarget).hasClass('show')) {
                e.preventDefault();
                $(e.delegateTarget).addClass('show');
                $(e.delegateTarget).find('input[type=text]').focus();
                $('body').one('click', function (e) {
                    $('.toolbar__search').removeClass('show');
                });
            }
        }
    });

    $('.toolbar__search').on('click', 'input', function (e) {
        e.stopPropagation();
    });
    
    var current_breakpoint = getCurrentBreakpoint();

    $(window).on('load', function() {
        updateUI();
        $('svg').wrap('<div aria-hidden="true"></div>');
    });

    $(window).on('resize', function() {

        var _cb = getCurrentBreakpoint();
        
        if (current_breakpoint !== _cb) {
            current_breakpoint = _cb;
            updateUI();
        }
        
    });

    function updateUI() {
        if (Modernizr.mq('(max-width: 991px)') && !isMobileView) {
            isMobileView = true;
            isMediumView = false;
            isLargeView = false;
            equalizeHeightsWithPadding('.modules-top .module');
            $('.header-address', '#banner').prependTo('.header-name', '#banner');
            $('.header-image-1').prependTo('.header-address', '#banner');
            $('.header-image-2').prependTo('.header-welcome .message');
            $('#searchbar').insertAfter('#fonts');
            $('#content').prependTo('#content-mobile');
            $('.header__title, .header__address').prependTo('.header__holder--mobile');
            $('#sidebar-menu').prependTo('#sidebar-menu-mobile');
            $('.modules-top').prependTo('#modules-top-mobile');
            $('#modules-top2').prependTo('#modules-top2-mobile');
            $('#modules-bottom').prependTo('#modules-bottom-mobile');
        } 
        else if (Modernizr.mq('(min-width: 992px)') && isMobileView) {
            isMobileView = false;
            $('.header-address', '#banner').insertAfter('.header-name', '#banner');
            $('.header-image-1').insertAfter('.header-address', '#banner');
            $('.header-image-2').insertAfter('.header-address', '#banner');
            $('#searchbar').prependTo('#searchbar-desktop');
            $('#content').prependTo('#content-desktop');
            $('.header__title, .header__address').prependTo('.header__holder--desktop');
            $('#sidebar-menu').prependTo('#sidebar-menu-desktop');
            $('.modules-top').prependTo('#modules-top-desktop');
            $('#modules-top2').prependTo('#modules-top2-desktop');
            $('#modules-bottom').prependTo('#modules-bottom-desktop');
        }
        if (Modernizr.mq('(min-width: 1200px)') && !isLargeView) {
            isLargeView = true;
            isMediumView = false;
            equalizeHeightsWithPadding('.modules-top .module');
        }
        else if (Modernizr.mq('(min-width: 992px)') && Modernizr.mq('(max-width: 1199px)') && !isMediumView) {
            isMediumView = true;
            isLargeView = false;
            equalizeHeightsWithPadding('.modules-top .module');
        }
        if (Modernizr.mq('(min-width: 768px)') && Modernizr.mq('(max-width: 991px)')) {
            equalizeHeightsWithPadding('.sidebar-modules .module');
        }
    }

    function specialModules() {
        var forum = $('.sidebar-modules #mod_forum').length;
        var contact = $('.sidebar-modules #mod_contact').length;
        var modulesTop2 = $('.modules-top2 > .row').children().length;
        var singleModulesTop2 = $('.modules-top2 .col-sm-12').length;
        if (forum && contact && modulesTop2) {
            $('.modules-top2 > .row').children().removeClass('col-sm-12 col-sm-6 col-sm-4').addClass('col-sm-4');    
            $('.sidebar-modules #mod_contact a').clone().addClass('module special--contact').appendTo('.modules-top2 > .row').wrap('<div class="col-sm-4 special"></div>');
            $('.sidebar-modules #mod_forum a').clone().addClass('module special--forum').appendTo('.modules-top2 > .row').wrap('<div class="col-sm-4 special"></div>');
            $('.modules-top2 .special--contact .modules-content__image pattern').attr('id', 'sidebar-image-contact-2');
            $('.modules-top2 .special--forum .modules-content__image pattern').attr('id', 'sidebar-image-forum-2');
            $('.modules-top2 .special--contact .modules-content__image path').attr('fill', 'url(#sidebar-image-contact-2)');
            $('.modules-top2 .special--forum .modules-content__image path').attr('fill', 'url(#sidebar-image-forum-2)');

        } 
        else if ((forum && !contact && modulesTop2 && singleModulesTop2) || (!forum && contact && modulesTop2 && singleModulesTop2)) {
            $('.modules-top2 > .row').children().removeClass('col-sm-12 col-sm-6 col-sm-4').addClass('col-sm-6'); 
            $('.sidebar-modules #mod_contact a').clone().addClass('module special--contact').appendTo('.modules-top2 > .row').wrap('<div class="col-sm-6 special"></div>');
            $('.sidebar-modules #mod_forum a').clone().addClass('module special--forum').appendTo('.modules-top2 > .row').wrap('<div class="col-sm-6 special"></div>');
            $('.modules-top2 .special--contact .modules-content__image pattern').attr('id', 'sidebar-image-contact-2');
            $('.modules-top2 .special--forum .modules-content__image pattern').attr('id', 'sidebar-image-forum-2');
            $('.modules-top2 .special--contact .modules-content__image path').attr('fill', 'url(#sidebar-image-contact-2)');
            $('.modules-top2 .special--forum .modules-content__image path').attr('fill', 'url(#sidebar-image-forum-2)');
        }
        else if ((forum && !contact && modulesTop2 && !singleModulesTop2) || (!forum && contact && modulesTop2 && !singleModulesTop2)) {
            $('.modules-top2 > .row').children().removeClass('col-sm-12 col-sm-6 col-sm-4').addClass('col-sm-4'); 
            $('.sidebar-modules #mod_contact a').clone().addClass('module special--contact').appendTo('.modules-top2 > .row').wrap('<div class="col-sm-4 special"></div>');
            $('.sidebar-modules #mod_forum a').clone().addClass('module special--forum').appendTo('.modules-top2 > .row').wrap('<div class="col-sm-4 special"></div>');
            $('.modules-top2 .special--contact .modules-content__image pattern').attr('id', 'sidebar-image-contact-2');
            $('.modules-top2 .special--forum .modules-content__image pattern').attr('id', 'sidebar-image-forum-2');
            $('.modules-top2 .special--contact .modules-content__image path').attr('fill', 'url(#sidebar-image-contact-2)');
            $('.modules-top2 .special--forum .modules-content__image path').attr('fill', 'url(#sidebar-image-forum-2)');
        }
        else if (forum && contact && !modulesTop2) {
            $('.sidebar-modules #mod_contact a').clone().addClass('module special--contact').appendTo('.modules-top2 > .row').wrap('<div class="col-sm-6 special"></div>');
            $('.sidebar-modules #mod_forum a').clone().addClass('module special--forum').appendTo('.modules-top2 > .row').wrap('<div class="col-sm-6 special"></div>');
            $('.modules-top2 .special--contact .modules-content__image pattern').attr('id', 'sidebar-image-contact-2');
            $('.modules-top2 .special--forum .modules-content__image pattern').attr('id', 'sidebar-image-forum-2');
            $('.modules-top2 .special--contact .modules-content__image path').attr('fill', 'url(#sidebar-image-contact-2)');
            $('.modules-top2 .special--forum .modules-content__image path').attr('fill', 'url(#sidebar-image-forum-2)');
        }
        else if ((forum && !contact && !modulesTop2) || (!forum && contact && !modulesTop2)) {
            $('.sidebar-modules #mod_contact a').clone().addClass('module special--contact').appendTo('.modules-top2 > .row').wrap('<div class="col-sm-12 special"></div>');
            $('.sidebar-modules #mod_forum a').clone().addClass('module special--forum').appendTo('.modules-top2 > .row').wrap('<div class="col-sm-12 special"></div>');
            $('.modules-top2 .special--contact .modules-content__image pattern').attr('id', 'sidebar-image-contact-2');
            $('.modules-top2 .special--forum .modules-content__image pattern').attr('id', 'sidebar-image-forum-2');
            $('.modules-top2 .special--contact .modules-content__image path').attr('fill', 'url(#sidebar-image-contact-2)');
            $('.modules-top2 .special--forum .modules-content__image path').attr('fill', 'url(#sidebar-image-forum-2)');
        }
    }

    specialModules();

    function moduleHeightAlign() {
        if (Modernizr.mq('(min-width: 768px)') && Modernizr.mq('(max-width: 991px)')) {

            var maxHeight = 0;
            var maxHeightModuleId = null;

            $('.sidebar-modules').find('.module').each(function () {
                var $this = $(this);
                if ($this.find('.module-body').height() > maxHeight) {
                    maxHeight = $this.find('.module-body').outerHeight();
                    maxHeightModuleId = $this.attr('id');
                }
            });

            $('.sidebar-modules').find('.module').each(function () {
                var $this = $(this);

                var moduleId = $this.attr('id');

                if (maxHeightModuleId !== moduleId) {

                    $this.find('.module-body').outerHeight(maxHeight);

                }
            });

            isModuleHeightAlign = true;

        } else if (isModuleHeightAlign) {

            $('.sidebar-modules').find('.module').each(function () {
                var $this = $(this);
                $this.outerHeight('auto');

            });

            isModuleHeightAlign = false;
        }
    }

    $('#navbar-top').on('click', '.dropdown-submenu > a', function (e) {

        var $this = $(this);
        var $submenu = $this.next('.dropdown-menu');

        if ($submenu.length > 0) {
            if (isMobile.any) {

                if ($this.parent().hasClass('open')) {
                    window.location.href = $this.attr('href');
                }

                $this.parent().addClass('open');

            } else {
                if ($this.parent().hasClass('open')) {
                    window.location.href = $this.attr('href');
                }
            }
        }

    });

    $('#sidebar-menu').on('click', 'a', function (e) {
        $(window).trigger('resize');

        var $this = $(this);
        var $sublist = $this.next('.dropdown-menu');

        if ($sublist.is(':hidden')) {
            e.preventDefault();
            $sublist.show();
        }
        else if (!$sublist.is(':hidden')) {
            window.location.href = $this.attr('href');
        }

    });

    $('.menus a.selected', '#sidebar-menu').parents('.dropdown-menu').show();
    
});

$(function() {
    var a = new Date,
        b = new Date;
    $(document).on("focusin", function(c) {
        $(".keyboard-outline").removeClass("keyboard-outline");
        var d = b < a;
        d && $(c.target).addClass("keyboard-outline");
    }), $(document).on("click", function() {
        b = new Date;
    }), $(document).on("keydown", function() {
        a = new Date;
    });
});

function getCurrentBreakpoint() {
    
    var breakpoints = [0, 768, 992, 1200].reverse();
    
    for (var i=0; i < breakpoints.length; i++) {
        if (Modernizr.mq('(min-width: ' + breakpoints[i] + 'px)')) {
            return i;
        }
    }
}
