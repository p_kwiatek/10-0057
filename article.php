<h2 class="main-header"><span><?php echo $pageName?></span></h2>
<div class="main-text">
<?php
echo $message;

if ($showLoginForm)
{
    include( CMS_TEMPL . DS . 'form_login.php');
}
		
if ($showArticle)
{
    echo '<div class="leadArticle">' . $article['lead_text'] . '</div>';
    
    echo $article['text'];
		
    if (! check_html_text($article['author'], '') )
    {
	?>
	<p class="author-name"><?php echo __('author')?>: <?php echo $article['author']?></p>
	<?php
    }
    
    ?>
</div>
<div>
    <?php
    /*
     *  Wypisanie plikow do pobrania
     */
    if ($numFiles > 0)
    {
	?>
	<div class="files-wrapper row">
            <div class="col-xs-12">
                <h3 class="files-header"><?php echo __('files')?></h3>
                <ul class="list-unstyled">
                <?php
                foreach ($outRowFiles as $row)
                {
                    $target = 'target="_blank" ';

                    if (filesize('download/'.$row['file']) > 5000000)
                    {
                        $url = 'download/'.$row['file'];
                    } else
                    {
                        $url = 'index.php?c=getfile&amp;id='.$row['id_file'];
                    }
                    if (trim($row['name']) == '')
                    {
                        $name = $row['file'];
                    } else
                    {
                        $name = $row['name'];
                    }			
                    $size = file_size('download/'.$row['file']);	
                    ?>
                    <li>
                        <a href="<?php echo $url?>" <?php echo $target?>>
                            <i class="icon-doc-text-inv icon" aria-hidden="true"></i>
                            <span class="title">
                                <?php echo $name?>
                                <span class="size">(<?php echo $size?>)</span>
                            </span>
                        </a>
                    </li>
                    <?php
                }
                ?>
                </ul>
            </div>
	</div>
    <?php
    }
		
    /*
     *  Wypisanie zdjec
     */
    if ($numPhotos > 0)
    {	
	$i = 0;
	?>
	<div class="gallery-wrapper row">
            <div class="col-xs-12">
                <h3 class="gallery-header"><?php echo __('gallery')?></h3>
                <ul class="list-unstyled row gallery">
                <?php
                foreach ($outRowPhotos as $row)
                {
                    $i++;
                    $noMargin = '';
                    if ($i == $pageConfig['zawijaj'])
                    {
                        $noMargin = ' noMargin';
                    }
                    ?>
                    <li class="col-sm-6 <?php echo $noMargin?>">
                        <a href="files/<?php echo $lang?>/<?php echo $row['file']?>" title="<?php echo __('enlarge image') . ': ' . $row['name']?>" data-fancybox-group="gallery" class="photo fancybox">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 360 240" class="gallery-wrapper__shadow">
                                <path fill-rule="evenodd"  opacity="0.302"
                             d="M10.000,-0.000 L360.000,-0.000 L360.000,240.000 L-0.000,240.000 C6.000,191.000 10.000,-0.000 10.000,-0.000 Z"/>
                            </svg>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 360 250" class="gallery-wrapper__image">
                                <defs>
                                    <pattern id="<?php echo 'article-image-' . $i; ?>" patternUnits="userSpaceOnUse" preserveAspectRatio="xMidYMid slice" width="100%" height="100%" x="0" y="0">
                                        <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $row['file']?>"></image>
                                    </pattern>
                                </defs>
                                <path fill-rule="evenodd" fill="<?php echo 'url(#article-image-' . $i . ')'; ?>" d="M-0.000,-0.000 L360.000,-0.000 L360.000,250.000 L-0.000,250.000 L-0.000,-0.000 Z"/>
                            </svg>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="photo-wrapper__paperclip--shadow">
                                <path d="M74.2,62.5c-1.7,3.7-5.6,5.7-9.5,5.2l0.3-2.1c3,0.4,5.9-1.2,7.3-4   c0.8-1.7,0.9-3.6,0.2-5.3c-0.6-1.8-1.9-3.1-3.6-3.9L34,36.3c-1.2-0.6-2.5-0.6-3.8-0.1c-1.2,0.5-2.2,1.4-2.8,2.6   c-1.1,2.5-0.1,5.4,2.4,6.5l27.4,12.6c1.4,0.7,3.2,0,3.8-1.4c0.7-1.4,0-3.2-1.4-3.8L42,44.5c-0.5-0.2-0.8-0.9-0.5-1.4   c0.2-0.5,0.9-0.8,1.4-0.5l17.6,8.1c2.5,1.2,3.6,4.1,2.5,6.7c-1.2,2.5-4.1,3.6-6.7,2.5L28.9,47.2c-1.8-0.8-3-2.3-3.7-4   c-0.6-1.7-0.6-3.6,0.2-5.4c1.6-3.5,5.8-5.1,9.4-3.5l34.8,16.1c2.2,1,3.9,2.8,4.7,5.1C75.3,57.8,75.2,60.3,74.2,62.5z"/>
                            </svg>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="photo-wrapper__paperclip">
                                <path d="M74.2,62.5c-1.7,3.7-5.6,5.7-9.5,5.2l0.3-2.1c3,0.4,5.9-1.2,7.3-4   c0.8-1.7,0.9-3.6,0.2-5.3c-0.6-1.8-1.9-3.1-3.6-3.9L34,36.3c-1.2-0.6-2.5-0.6-3.8-0.1c-1.2,0.5-2.2,1.4-2.8,2.6   c-1.1,2.5-0.1,5.4,2.4,6.5l27.4,12.6c1.4,0.7,3.2,0,3.8-1.4c0.7-1.4,0-3.2-1.4-3.8L42,44.5c-0.5-0.2-0.8-0.9-0.5-1.4   c0.2-0.5,0.9-0.8,1.4-0.5l17.6,8.1c2.5,1.2,3.6,4.1,2.5,6.7c-1.2,2.5-4.1,3.6-6.7,2.5L28.9,47.2c-1.8-0.8-3-2.3-3.7-4   c-0.6-1.7-0.6-3.6,0.2-5.4c1.6-3.5,5.8-5.1,9.4-3.5l34.8,16.1c2.2,1,3.9,2.8,4.7,5.1C75.3,57.8,75.2,60.3,74.2,62.5z"/>
                            </svg>
                            <?php
                            if (! check_html_text($row['name'], '') )
                            {
                                ?>
                                <p class="photo-name" aria-hidden="true"><?php echo $row['name']?></p>
                                <?php
                            }
                            ?>
                        </a>
                    </li>
                <?php
                }
                ?>
                </ul>
            </div>
	</div>
    <?php
    }		
    if ($outSettings['pluginTweet'] == 'włącz')
    {
	 echo '<div class="Tweet"><iframe frameborder="0" scrolling="no" src="//platform.twitter.com/widgets/tweet_button.html" style="width:80px; height:30px;"></iframe></div>';  
    }

    if ($outSettings['pluginFB'] == 'włącz')
    {
        $color = 'light';
        if ($_SESSION['contr'] == 1)
        {
            $color = 'dark';
        }        
	$fb_url = urlencode('http://'.$pageInfo['host'].'/index.php?c=article&amp&id='. $_GET['id']);
	echo '<div class="FBLike"><iframe src=\'http://www.facebook.com/plugins/like.php?href='.$fb_url.'&amp;layout=standard&amp;show_faces=true&amp;width=400&amp;action=like&amp;font=tahoma&amp;colorscheme='.$color.'&amp;height=32&amp;show_faces=false\' scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:400px; height:32px;"></iframe></div>';   
    }
}
?>
</div>