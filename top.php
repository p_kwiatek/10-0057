<a id="top"></a>    
<ul class="skip-links list-unstyled">
    <li><a href="#main-menu"><?php echo __('skip to main menu')?></a></li> 
    <li><a href="#add-menu"><?php echo __('skip to additional menu')?></a></li> 
    <li><a href="#link-content"><?php echo __('skip to content')?></a></li>
    <li><a href="#search"><?php echo __('skip to search')?></a></li>
    <li><a href="mapa_strony"><?php echo __('sitemap')?></a></li>
</ul>

<div id="popup"></div>

<div class="top">
    <header class="header">
        <h1 class="sr-only"><?php echo $pageInfo['name']; ?></h1>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php
                        include_once ( CMS_TEMPL . DS . 'toolbar.php');
                    ?>
                    <div class="header__holder--desktop">
                        <div class="header__title">
                            <?php
                            $pageInfo['name'] = str_replace('w ', 'w&nbsp;', $pageInfo['name']);
                            $pageInfo['name'] = str_replace('im ', 'im&nbsp;', $pageInfo['name']);
                            ?>
                            <p><?php echo $pageInfo['name']; ?></p>
                        </div>
                        <?php
                        if (strlen($pageInfo['name']) > 60) {
                        ?>
                            <div class="header__address small">
                                <?php
                                echo $headerAddress;
                                if ($pageInfo['email'] != '') {
                                    echo '<a href="mailto:' . $pageInfo['email'] . '">' . $pageInfo['email'] . '</a>';
                                }
                                ?>
                            </div>
                        <?php
                        }
                        else {
                        ?>
                            <div class="header__address">
                                <?php
                                echo $headerAddress;
                                if ($pageInfo['email'] != '') {
                                    echo '<a href="mailto:' . $pageInfo['email'] . '">' . $pageInfo['email'] . '</a>';
                                }
                                ?>
                            </div>
                        <?php
                        }
                        ?> 
                    </div>
                </div>
                <div class="col-md-9 clearfix">
                    <div id="banner" class="banner">
                        <?php if ($outBannerTopRows == 0): ?>
                            <?php
                                $img = $templateDir . '/' . 'images' . '/' . 'banner-default.jpg';
                            ?>
                            <div class="carousel-content">
                                <div class="banner-photo">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 846.874 430.25" class="shadow" preserveAspectRatio="none">
                                        <path opacity="0.3" d="M535.237,35.874l785.243,7.139,50.55,403.853c-227.24,16.387-632.615,21.9-846.877,18.159C531.609,310.5,544.462,191.137,535.237,35.874Z" transform="translate(-524.156 -35.875)"/>
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 846.592 438.813">
                                        <pattern id="header-banner" patternUnits="objectBoundingBox" width="100%" height="100%">
                                            <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img; ?>"></image>
                                        </pattern>
                                        <path fill="url(#header-banner)" d="M523.927,26.269L1351.76,14.884l18.78,430.674c-244.74,9.332-612.935,12.281-825.435-.183C552.649,288.28,533.272,184.125,523.927,26.269Z" transform="translate(-523.938 -14.875)"></path>
                                    </svg>
                                </div>
                            </div>
                        <?php elseif ($outBannerTopRows == 1): ?>
                            <?php
                                $value = $outBannerTop[0];
                                $img = 'files' . '/' . $lang . '/' . 'mini' . '/' . urldecode($value['photo']);
                            ?>
                            <div class="carousel-content">
                                <div class="banner-photo">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 846.874 430.25" class="shadow" preserveAspectRatio="none">
                                        <path opacity="0.3" d="M535.237,35.874l785.243,7.139,50.55,403.853c-227.24,16.387-632.615,21.9-846.877,18.159C531.609,310.5,544.462,191.137,535.237,35.874Z" transform="translate(-524.156 -35.875)"/>
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 846.592 438.813">
                                        <pattern id="header-banner" patternUnits="objectBoundingBox" width="100%" height="100%">
                                            <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img; ?>"></image>
                                        </pattern>
                                        <path fill="url(#header-banner)" d="M523.927,26.269L1351.76,14.884l18.78,430.674c-244.74,9.332-612.935,12.281-825.435-.183C552.649,288.28,533.272,184.125,523.927,26.269Z" transform="translate(-523.938 -14.875)"></path>
                                    </svg>
                                </div>
                            </div>
                        <?php else: $i; ?>
                            <div class="carousel-content owl-carousel">
                                <?php foreach ($outBannerTop as $value): ?>
                                    <?php
                                        $i++;
                                        $img = 'files' . '/' . $lang . '/' . 'mini' . '/' . urldecode($value['photo']);
                                    ?>
                                    <div class="banner-photo">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 846.874 430.25" class="shadow" preserveAspectRatio="none">
                                            <path opacity="0.3" d="M535.237,35.874l785.243,7.139,50.55,403.853c-227.24,16.387-632.615,21.9-846.877,18.159C531.609,310.5,544.462,191.137,535.237,35.874Z" transform="translate(-524.156 -35.875)"/>
                                        </svg>
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 846.592 438.813">
                                            <pattern id="<?php echo 'header-banner-' . $i; ?>" patternUnits="objectBoundingBox" width="100%" height="100%">
                                                <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img; ?>"></image>
                                            </pattern>
                                            <path fill="<?php echo 'url(#header-banner-' . $i . ')'; ?>" d="M523.927,26.269L1351.76,14.884l18.78,430.674c-244.74,9.332-612.935,12.281-825.435-.183C552.649,288.28,533.272,184.125,523.927,26.269Z" transform="translate(-523.938 -14.875)"></path>
                                        </svg>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="header__holder--mobile"></div>
                </div>
            </div>
        </div>
    </header>
    <div id="menu-top" class="menu-top">
        <a id="main-menu" tabindex="-1" style="display: inline;"></a>
        <nav class="navbar">
            <p class="sr-only"><?php echo __('main menu'); ?></p>
            <div class="navbar-header">
                <button class="navbar-toggle collapsed" aria-controls="navbar-top" aria-expanded="false" data-target="#navbar-top" id="navbar-mobile" data-toggle="collapse" type="button">
                    <i class="icon" aria-hidden="true"></i>
                    <i class="icon" aria-hidden="true"></i>
                    <i class="icon" aria-hidden="true"></i>
                    <span class="sr-only"><?php echo __('expand')?></span>
                    <span class="title sr-only"><?php echo __('main menu')?></span>
                </button>
            </div>
            <div id="navbar-top" class="navbar-collapse menu">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 holder">
                            <?php get_menu_tree('tm', 0, 0, '', false, '', false, true, true, false, true, true, '', true); ?>
                        </div>
                    </div>
                </div>
            </div>        
        </nav>
    </div>
    <div class="page-content page-content-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="content-mobile"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div id="sidebar-menu-mobile" class="sidebar"></div>
                </div>
            </div>
        </div>
    </div>
</div>
